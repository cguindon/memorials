# In Memory of Daniel Megert



Please add your contribution below via a merge request. 

## Denis Roy

Daniel was passionate about all things Eclipse, and worked tirelessly to make things better. Easily approachable and personable, he would engage constructively to challenge status quo and was very understanding of the constraints that were slowing progress. 

## Jonah Graham

Thank you to Dani for your tireless work on Eclipse. Your absence will be sorely missed. You leave behind an excellent legacy of a mature and stable Eclipse project that you dedicated so many years of your life contributing to and improving.

## Mickael Istria

You were not only a great technical leader who always managed to drive the community towards the best solutions; but more importantly  an open-minded, listening, trusting and sharing individual who acted as a mentor for a lot of contributors -including me- and made them better in all the way software development can need. You've been the project lead who has allowed the Eclipse Platform and JDT project to become more active, more diverse, more exciting and more relevant in the most difficult time; most of the good things that are happening today are the result of your efforts combined with your very pleasant personality. Thanks eternally for all that!

## Martin Lippert

I remember my first meeting with Dani, it was more than 20 years ago, at the University of Hamburg, when I was working there as a research assistent and he visited the group with a number of folks from OTI back then. I was deeply impressed and had a ruge respect for that group, their work, and their deep understanding and knowledge of their domain, the developer tools and IDE space. I enjoyed staying in contact with them, got great and always constructive feedback from them, especially from Dani - always friendly, always constructive, and to the point. I very much enjoyed meeting him again and again at EclipseCon conferences and staying in contact over all the years. Thank you again, Dani, for all your feedback and friendship over years.
